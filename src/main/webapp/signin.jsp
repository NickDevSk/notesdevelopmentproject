<%--
  Created by IntelliJ IDEA.
  User: nick
  Date: 10.03.17
  Time: 1:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SignIn</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
            <div class="card card-block">
                <h3 class="text-xs-center">Sign In</h3>
                <hr>
                <form action="" method="post">
                    <fieldset>
                        <div class="form-group has-success">
                            <input class="form-control input-lg" placeholder="Name" name="name" value="" type="text">
                        </div>
                        <div class="form-group has-success">
                            <input class="form-control input-lg" placeholder="Password" name="pass" value="" type="password">
                        </div>
                        <input class="btn btn-lg btn-primary btn-block" value="Sign Me In" type="submit">
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
