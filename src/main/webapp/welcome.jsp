<%--
  Created by IntelliJ IDEA.
  User: nick
  Date: 08.03.17
  Time: 23:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <style type="text/css">
        body {
            background-image: url('http://crunchify.com/bg.png');
        }
    </style>

    <title>Spring MVC: How to Access ModelMap Values in a JSP? -
        Crunchify.com</title>
    <meta rel="author" href="http://crunchify.com">
</head>

<body>
<div align="center">
    <br>
    <h1>${heading}</h1>
    <h2>{result1}</h2>
    <br>
    <h3>${result2}</h3>
    <br> <br> ${credit}
</div>
</body>

</html>