<%--
  Created by IntelliJ IDEA.
  User: nick
  Date: 10.03.17
  Time: 0:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SignUp</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
            <div class="card card-block">
                <h3 class="text-xs-center">Register Account</h3>
                <hr>
                <form action="" method="post">
                <fieldset>
                    <div class="form-group has-error">
                        <input class="form-control input-lg" placeholder="E-mail Address" name="email" type="text">
                    </div>
                    <div class="form-group has-success">
                        <input class="form-control input-lg" placeholder="Name" name="name" value="" type="text">
                    </div>
                    <div class="form-group has-success">
                        <input class="form-control input-lg" placeholder="Password" name="pass" value="" type="password">
                    </div>
                    <div class="form-group has-success">
                        <input class="form-control input-lg" placeholder="Confirm Password" name="checkpass" value="" type="password">
                    </div>
                    <div class="checkbox">
                        <label class="small">
                            <input name="terms" type="checkbox">I have read and agree to the <a href="#">terms of service</a>
                        </label>
                    </div>
                    <input class="btn btn-lg btn-primary btn-block" value="Sign Me Up" type="submit">
                </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
