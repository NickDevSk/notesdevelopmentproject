package com.vertexoop.controller;

import com.vertexoop.model.article.Article;
import com.vertexoop.model.article.ArticleJDBCTemplate;
import com.vertexoop.model.user.User;
import com.vertexoop.model.user.UserJDBCTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class HomeController {

    @RequestMapping(value = "/hello")
    public String home(){
        return "test";
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public @ResponseBody String updateUser(@PathVariable("id") long id, @RequestParam String name, @RequestParam String email, @RequestParam String pass){
        User user = new User();
        user.setId(id);
        user.setName(name);
        user.setEmail(email);
        user.setPass(pass);
        System.out.println(user.toString());
        return user.toString();
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signUpCreate(@RequestParam String name, @RequestParam String pass, @RequestParam String email){
        if (name!=null && pass!=null && email!=null) {
            ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
            UserJDBCTemplate studentJDBCTemplate =
                    (UserJDBCTemplate) context.getBean("userJDBCTemplate");
            try {
                studentJDBCTemplate.create(name, email, pass);
                return "main";
            } catch (Exception e) {
                return "signup";
            }
        }else {
        return "signup";
        }
    }

    @RequestMapping(value = "/signup")
    public String signUp(){
        return "signup";
    }

    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public String signInCheck(@RequestParam String name, @RequestParam String pass, ModelMap model){
        if (name!=null && pass!=null) {
            ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
            UserJDBCTemplate userJDBCTemplate =
                    (UserJDBCTemplate) context.getBean("userJDBCTemplate");
            User user;
            if((user = userJDBCTemplate.checkUser(name, pass))!=null){
                model.addAttribute("user", user);
                return main(model);
            }
            else
                return "signup";
        }else
            return "signup";
    }

    @RequestMapping(value = "/signin")
    public String signIn(){
        return "signin";
    }

    @RequestMapping(value = "/main")
    public String main(ModelMap model){
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        ArticleJDBCTemplate articleJDBCTemplate =
                (ArticleJDBCTemplate)context.getBean("articleJDBCTemplate");
        List<Article> articles;
        if (!(articles = articleJDBCTemplate.listArticles()).isEmpty()){
            model.addAttribute("articles", articles);
            return "main";
        }else
            return "notfound";
    }

    @RequestMapping(value = "/arc/{status}", method = RequestMethod.GET)
    public String getArticle(@PathVariable("status") long status, @RequestParam long id, ModelMap model){
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        ArticleJDBCTemplate articleJDBCTemplate =
                (ArticleJDBCTemplate)context.getBean("articleJDBCTemplate");
        Article article;
        if ((article = articleJDBCTemplate.getArticle(id))!=null){
            model.addAttribute("article", article);
            return "article";
        }else
            return "notfound";
    }
}
