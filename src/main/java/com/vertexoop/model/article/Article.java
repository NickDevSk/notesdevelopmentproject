package com.vertexoop.model.article;


public class Article {
    private long id;
    private String title;
    private String data;
    private long idUser;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @Override
    public String toString() {
        return
        String.format(
                "{id=%d, title='%s', data='%s', idUser='%d'}",
                id,title, data, idUser);
    }
}
