package com.vertexoop.model.article;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class ArticleJDBCTemplate implements ArticleDAO {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }
    public void create(String title, String data, Integer idUser) {
        String SQL = "insert into Article (title, data, idUser) values (?, ?, ?)";
        jdbcTemplateObject.update( SQL, title, data, idUser);
        System.out.println("Created Record idUser = " + idUser + " Title = " + title + " Data = " + data);
        return;
    }
    public Article getArticle(Long id) {
        String SQL = "select * from Article where id = ?";
        Article user = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new ArticleMapper());
        return user;
    }
    public Article checkArticle(String title, String idUser) {
        String SQL = "select * from Article where title = ? and idUser= ?";
        try {
            Article user = jdbcTemplateObject.queryForObject(SQL,
                    new Object[]{title, idUser}, new ArticleMapper());
            return user;
        } catch (Exception e) {
            return null;
        }
    }
    public List<Article> listArticles() {
        String SQL = "select * from Article";
        List <Article> articles = jdbcTemplateObject.query(SQL, new ArticleMapper());
        return articles;
    }
    public void delete(Integer id) {
        String SQL = "delete from Article where id = ?";
        jdbcTemplateObject.update(SQL, id);
        System.out.println("Deleted Record with ID = " + id );
        return;
    }
    public void updateArticle(Integer id, String title, String data){
        String SQL = "update Article set title = ?, data = ? where id = ?";
        jdbcTemplateObject.update(SQL, title, data, id);
        System.out.println("Updated Email Record with ID = " + id );
        return;
    }
}