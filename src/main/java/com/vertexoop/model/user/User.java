package com.vertexoop.model.user;


public class User {
    private long id;
    private String pass;
    private String name;
    private String email;

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return
        String.format(
                "{id=%d, name='%s', email='%s', pass='%s'}",
                id, name, email, pass);
    }
}
