package com.vertexoop.model.user;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

public class UserJDBCTemplate implements UserDAO {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }
    public void create(String name, String email, String pass) {
        String SQL = "insert into User (name, email, pass) values (?, ?, ?)";
        jdbcTemplateObject.update( SQL, name, email, pass);
        System.out.println("Created Record Name = " + name + " Email = " + email + " Pass = " + pass);
        return;
    }
    public User getUser(Integer id) {
        String SQL = "select * from User where id = ?";
        User user = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new UserMapper());
        return user;
    }
    public User checkUser(String name, String pass) {
        String SQL = "select * from User where name = ? and pass= ?";
        try {
            User user = jdbcTemplateObject.queryForObject(SQL,
                    new Object[]{name, pass}, new UserMapper());
            return user;
        } catch (Exception e) {
            return null;
        }
    }
    public List<User> listUsers() {
        String SQL = "select * from User";
        List <User> users = jdbcTemplateObject.query(SQL, new UserMapper());
        return users;
    }
    public void delete(Integer id) {
        String SQL = "delete from User where id = ?";
        jdbcTemplateObject.update(SQL, id);
        System.out.println("Deleted Record with ID = " + id );
        return;
    }
    public void updateEmail(Integer id, String email){
        String SQL = "update User set email = ? where id = ?";
        jdbcTemplateObject.update(SQL, email, id);
        System.out.println("Updated Email Record with ID = " + id );
        return;
    }
    public void updatePass(Integer id, String pass){
        String SQL = "update User set pass = ? where id = ?";
        jdbcTemplateObject.update(SQL, pass, id);
        System.out.println("Updated Pass Record with ID = " + id );
        return;
    }
    public void updateName(Integer id, String name){
        String SQL = "update User set name = ? where id = ?";
        jdbcTemplateObject.update(SQL, name, id);
        System.out.println("Updated Name Record with ID = " + id );
        return;
    }
}